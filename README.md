# TouchEnchantments

TouchEnchantments offers an open-source service making your laptop, smartphone, tablet or 2in1 capable of detecting touch gestures.

The following gestures are available:

 - Long-tap for right click
 - Side-to-center for quick actions

The package will be packed as Debian packege RPM and EOPKG planed.
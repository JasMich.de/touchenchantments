#!/bin/bash
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi
systemctl disable /etc/systemd/system/touchenchantments.service
rm -rf /usr/share/TouchEnchantments/ /etc/TouchEnchantments/ /etc/systemd/system/touchenchantments.service

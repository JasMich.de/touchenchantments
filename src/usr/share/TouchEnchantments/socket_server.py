#!/usr/bin/env python3
import socket
import os

SOCKET_PATH="/run/touchenchantments.socket"

class TouchSocket():
    def __init__(self):
        if os.path.exists(SOCKET_PATH):
            os.remove(SOCKET_PATH)

        self.clients=[]

        print("Opening master socket...")
        self.server = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self.server.bind(SOCKET_PATH)
        os.chmod(SOCKET_PATH, 666)
        self.server.listen(1)
        conn, addr = self.server.accept()
        print("Server started at", SOCKET_PATH)
        print('Connected client.')
        data = conn.recv(1024)
        #if not data: break

        local=socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        local.connect(data.decode('utf-8'))

        self.clients.append(local)
        conn.close()


    def send(self, cmd):
        i = 0
        for i in self.clients:
            i.send(cmd.encode("utf-8"))

    def close(self):
        print("Shutting down...")
        self.server.close()
        os.remove(SOCKET_PATH)
        print("Done")

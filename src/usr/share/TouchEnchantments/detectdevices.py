#!/usr/bin/env python3

import evdev

def DetectTouchDevices():
    touch_screens=[]
    pens=[]
    
    devices = [evdev.InputDevice(path) for path in evdev.list_devices()]
    for device in devices:
        device_output=str(device.capabilities(verbose=True))
        if "BTN_TOUCH" in device_output and ("BTN_MOUSE" in device_output)==False and ("BTN_TOOL_PEN" in device_output)==False:
            if hasattr(device, "path"):
                touch_screens.append(device.path)
            else:
                touch_screens.append(device.fn)
        if "BTN_TOUCH" in device_output and ("BTN_MOUSE" in device_output)==False and "BTN_TOOL_PEN" in device_output:
            if hasattr(device, "path"):
                pens.append(device.path)
            else:
                pens.append(device.fn)
    
    touch_devices={
        "screens":touch_screens,
        "pens":pens
    }
    return(touch_devices)
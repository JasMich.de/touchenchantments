#!/usr/bin/env python3

import configparser
from pathlib import Path
home = str(Path.home())


class TouchConfigReader:

    def __init__(self):
        self.systemconfig = configparser.ConfigParser()
        self.systemconfig.read("/etc/TouchEnchantments/gestures.conf")
        #try:
        self.userconfig = configparser.ConfigParser()
        self.userconfig.read(home+"/.config/TouchEnchantments/gestures.conf")
        #except:
            #print("No user config available.")

    def getConfig(self, gesture, key):
        try:
            value=self.userconfig.get(gesture,key)
        except:
            #print("Key not available in user config")
            value=self.systemconfig.get(gesture,key)
        return(value)
